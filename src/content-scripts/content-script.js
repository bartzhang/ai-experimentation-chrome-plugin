import { MRDiff } from './mrdiff';
import { CreateOverlay} from './overlay';
import { InjectScripts } from './injector';

const extensionId = chrome?.runtime?.id;

// common scripts
const scriptsToInject = ['/js/chunk-common.js', '/js/chunk-vendors.js'];

// so Vue components know how to find the extensionId, set it on the body
document.getElementsByTagName("body")[0].setAttribute("gl-extension-id", extensionId);

// Create a container to house all scripts we need to inject
InjectScripts(scriptsToInject);

// Create an overlay to send general query/prompts
CreateOverlay(extensionId);

// Add your injections here
MRDiff(extensionId);
