import { CreateContainer, InjectComponent } from "./injector";

export function MRDiff(extensionId) {
    const appName = 'gl-ai-extension-mrdiff-app';
    const appJS = '/js/mrdiff.js' // This is the name of your feature(s) directory (e.g. src/mrdiff/main.js -> /js/mrdiff.js).
    const pageType = 'projects:merge_requests'; // matches all `projects:merge_request.* pages
    const locationSelector = 'div[class="file-header-content"]'; // your selector where to inject your component
    let container = function () { return CreateContainer(appName, extensionId) };
    InjectComponent(pageType, locationSelector, container, appJS);
}