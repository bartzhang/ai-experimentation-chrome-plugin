import axios from 'axios';
import { getPageValue } from './pageUtils';

const loadBlobContent = async function (url) {
  const newUrl = url.replace('/-/blob/', '/-/raw/');
  // Axios GET Default
  const response = await axios.get(newUrl);
  return response.data;
};

export async function loadBlobInfo(url, win) {
  const info = {};

  info.filePath = await getPageValue(win, "$$('body')[0].dataset.pageTypeId");
  const pathSplit = info.filePath.split('/');
  info.fileName = pathSplit[pathSplit.length - 1];

  info.code = await loadBlobContent(url, info);

  return info;
}
