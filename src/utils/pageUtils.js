export const getPageValue = function async (win, path) {
  return new Promise((resolve, reject) => {
    win.eval(path, async (res, isException) => {
      if (!isException) {
        resolve(res);
      } else {
        reject(isException);
      }
    });
  });
};

export const GetPageBody = function (callerType) {
  if (callerType === "content-script") {
    const body = document.getElementsByTagName('body');
    if (!body[0]) {
      console.error('failed to find body element dataset');
      return;
    }
    return body;
  }

}