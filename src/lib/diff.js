
export function ParseDiff(diff, fileChangesToExtract) {
    const lines = diff.split('\n');
    const parsed = [];
    let inFile = false;
    console.log('processing %o lines', lines.length);
    lines.forEach((l) => {
        let line = l.trim();
        if (line.startsWith("diff --git") && line.endsWith(fileChangesToExtract)) {
            inFile = true;    
        } else if (line.startsWith("diff --git")) {
             inFile = false;
        }

        if (inFile) {
             parsed.push(line);
        }
    });
    console.log('collected %o lines', parsed.length);
    return parsed.join("\n");
}