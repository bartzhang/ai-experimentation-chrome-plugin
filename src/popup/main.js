import Vue from 'vue'
import App from './App.vue'
import setConfigs from '@gitlab/ui/dist/config';

setConfigs();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
