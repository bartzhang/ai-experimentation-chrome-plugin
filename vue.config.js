const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,

  pages: {
    popup: {
      template: 'public/browser-extension.html',
      entry: './src/popup/main.js',
      title: 'Popup',
    },
    devtools_panel: {
      template: 'public/browser-extension.html',
      entry: './src/devtools/main.js',
      title: 'DevtoolsPanel',
    },
    overlay: {
      template: 'public/browser-extension.html',
      entry: './src/overlay/main.js',
      title: 'GL Experiment Overlay'
    },
    mrdiff: {
      template: 'public/browser-extension.html',
      entry: './src/mrdiff/main.js',
      title: 'GL Experiment MRDiff'
    }
  },

  pluginOptions: {
    browserExtension: {
      componentOptions: {
        background: {
          entry: 'src/background.js',
        },
        devtools: {
          entry: 'src/devtools.js',
        },
        contentScripts: {
          entries: {
            'content-script': ['src/content-scripts/content-script.js'],
          },
        },
      },
    },
  },

  configureWebpack: {
    devtool: 'cheap-module-source-map',
},
});
